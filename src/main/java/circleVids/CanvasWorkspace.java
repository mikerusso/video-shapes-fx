package circleVids;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;


import org.bytedeco.javacv.Frame;
// import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.Java2DFrameUtils;

import circleVids.utils.Circle;
import circleVids.utils.frameGrabber;
import circleVids.utils.FastRGB;

public class CanvasWorkspace {

    BufferedImage bimg;
    WritableRaster wr;
    int width;
    int height;
    Circle circle1;
    Circle circle2;
    Circle circle3;
    Circle circle4;
    int aPixel;

    // Java2DFrameConverter converter1 = new Java2DFrameConverter();
    // Java2DFrameConverter converter2 = new Java2DFrameConverter();
    // Java2DFrameConverter converter3 = new Java2DFrameConverter();
    // Java2DFrameConverter converter4 = new Java2DFrameConverter();

    // BufferedImage grabImage1;
    // BufferedImage grabImage2;
    // BufferedImage grabImage3;
    // BufferedImage grabImage4;
    FastRGB grabImage1;
    FastRGB grabImage2;
    FastRGB grabImage3;
    FastRGB grabImage4;

    public CanvasWorkspace(int width, int height) {
        this.width = width;
        this.height = height;

        bimg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);// not sure if two is right yet see above
        wr = bimg.getRaster();
        circle1 = new Circle(height / 2, width / 2, height / 2);
        circle2 = new Circle(height / 2, width / 2, (height / 2) / 2);
        circle3 = new Circle(height / 2, width / 2, (height / 2) / 3);
        circle4 = new Circle(height / 2, width / 2, (height / 2) / 4);
        circle3.setOffset(0, -200);
        circle4.setOffset(0, -200);
    }

    public Frame update(frameGrabber grab1, frameGrabber grab2, frameGrabber grab3, frameGrabber grab4, int index) {
        Frame finalFrame;
        try {
            // grabImage1 = Java2DFrameUtils.toBufferedImage(grab1.getNextFrame());
            // grabImage2 = Java2DFrameUtils.toBufferedImage(grab2.getNextFrame());
            // grabImage3 = Java2DFrameUtils.toBufferedImage(grab3.getNextFrame());
            // grabImage4 = Java2DFrameUtils.toBufferedImage(grab4.getNextFrame());
            grabImage1 = new FastRGB( Java2DFrameUtils.toBufferedImage(grab1.getNextFrame()) );
            grabImage2 = new FastRGB( Java2DFrameUtils.toBufferedImage(grab2.getNextFrame()) );
            grabImage3 = new FastRGB( Java2DFrameUtils.toBufferedImage(grab3.getNextFrame()) );
            grabImage4 = new FastRGB( Java2DFrameUtils.toBufferedImage(grab4.getNextFrame()) );

        } catch (Throwable e) {
            System.out.println("Error " + e.getMessage());
            e.printStackTrace();
        }

        try {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (circle4.checkPointInside(x, y)) {
                        aPixel = grabImage4.getRGB(x + circle4.xoffGet(), y + circle4.yoffGet());
                    } else if (circle3.checkPointInside(x, y)) {
                        aPixel = grabImage3.getRGB(x + circle3.xoffGet(), y + circle3.yoffGet());
                    } else if (circle2.checkPointInside(x, y)) {
                        aPixel = grabImage2.getRGB(x + circle2.xoffGet(), y + circle2.yoffGet());
                    } else if (circle1.checkPointInside(x, y)) {
                        aPixel = grabImage1.getRGB(x + circle1.xoffGet(), y + circle1.yoffGet());
                    } else {
                        aPixel = 0;
                    }

                    bimg.setRGB(x, y, aPixel);
                }
            }
        } catch (Throwable e) {
            System.out.println("Error " + e.getMessage());
            e.printStackTrace();
        }

        finalFrame = Java2DFrameUtils.toFrame(bimg);
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        System.out.println(index + ":  " + finalFrame);
        return finalFrame;

    }
}
