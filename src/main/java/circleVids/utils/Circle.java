package circleVids.utils;

public class Circle{
    int x;
    int y;
    int radius;
    public int xoff;
    public int yoff;

    public Circle(int x, int y, int radius){
        this.x = x;
        this.y = y;
        this.radius = radius;
        setOffset(0, 0);
    }

    public void setOffset(int xoff, int yoff){
        this.xoff = xoff;
        this.yoff = yoff;
    }

    public int xoffGet() {
        return xoff;
    }

    public int yoffGet() {
        return yoff;
    }

    public boolean checkPointInside(int x, int y){
        int distanceSquared = (int)(Math.pow((x - this.x), 2) + Math.pow((y - this.y), 2));
        int radiusSquared = (int) Math.pow(this.radius,2);
        return distanceSquared <= radiusSquared ? true : false;
    }
}
