package circleVids.utils;

import java.util.Random;

import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.javacv.*;

public class frameGrabber {
    int currFrame = 0;
    int totalFrames;
    float start = 0;
    float end = 1;

    private int frameStart;
    private int frameEnd;

    private FFmpegFrameGrabber grabber;

    Random ran = new Random();

    public frameGrabber(String file) {
        try {
            grabber = FFmpegFrameGrabber.createDefault(file);
            grabber.setVideoCodec(avcodec.AV_CODEC_ID_MJPEG);
            grabber.start();
            totalFrames = grabber.getLengthInVideoFrames();
            frameStart = (int) (totalFrames * start);
            frameEnd = (int) (totalFrames * end);
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            e.printStackTrace();
        }
    }
    public int getFrameEnd(){
        return this.frameEnd;
    }
    public void flush(){
        try {
            grabber.restart();
        }
        catch (Throwable e) {
            System.out.println("Error " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Frame getFrame(int frame) {
        Frame tempFrame = null;
        try {
            grabber.setFrameNumber(frame);
            tempFrame = grabber.grab();
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            e.printStackTrace();
        }
        return tempFrame;
    }

    public Frame randomFrame() {
        int randomFrame = (int) ran.nextInt(totalFrames);
        return getFrame(randomFrame);
    }

    public void setRange(float start, float end) {
        this.start = start;
        this.end = end;
        frameStart = (int) (totalFrames * this.start);
        frameEnd = (int) (totalFrames * this.end)-2;
    }

    public Frame getNextFrame() {
        if (currFrame > frameEnd) {
            currFrame = frameStart;
        } else {
            currFrame++;
        }
        return getFrame(currFrame);
    }
}
